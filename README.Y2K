		    Super and Y2000 compliance.

Super is not commercial software and I don't supply any guarantees.
That said, however, I am confident that it contains no Y2000 bugs.

The software is coded using the "time_t" typedef to declare the time-like
variables, and it obtains the current time using the ANSI-standard time(3)
function.  Super converts time_t values into string form by using the
ANSI-standard C functions ctime(3) and localtime(3), and it never uses
the year number.  Therefore, the time values used by super(1) will be
as accurate as the underlying system.

In other words, super is Y2K-safe on any POSIX-compatible Unix.

    [[ If you have built super(1) on a system that does _not_ supply the
    standard ctime() and localtime() functions, then super will supply
    a function that assumes days can be computed by dividing the time_t
    value by 86400, and super's accuracy depends on the correctness of
    this assumption. ]]

FAQ:

Q1. Will the software will operate correctly through the year 2038, at least?
    (Note that if your time_t is a 32-bit signed quantity, a time
    value cannot store time past approximately 03h 19-jan-2038.)

A1. Yes.

    Super will work correctly as long as the system-supplied types and
    functions time_t, time(), ctime(), and localtime() will work;
    compiling super on a "Year-2038-compliant" system will produce a
    version of super that will be Y2038-safe.

-------------

Q2. Will the software will operate properly and without incident 
    if it is running between 23:59:59 31 Dec 1999 and 00:00:00 1 Jan 2000.

A2. Yes.

-------------

Q3. Does the software, in and of itself, understands dates after the
    year 1999, and correctly interpret them as such?

A3. Not applicable: super doesn't use year numbers at all.

    Super is concerned only with the current time of day and day of week.
    It has no year numbers, and no "century" digits on which
    it can make mistakes.

