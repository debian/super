#ifndef HSEARCH_H
#define HSEARCH_H

#include "config.h"

typedef struct entry {
    char           *key;
    char           *data;
}               ENTRY;

typedef enum {
    FIND,
    ENTER
}               ACTION;

typedef struct element {
    ENTRY           item;
    struct element *next;
}               ELEMENT;

extern ENTRY   *s_hsearch P__(( int which, ENTRY e, ACTION a ));
extern void     s_hdestroy P__(( int which ));
extern int      s_hcreate P__(( int which, unsigned int how_many ));
extern void     hprint P__(( int which, void (*f)() ));
extern void     htext P__(( int indx, char *key, char *data ));

#define HS_VARS  0	/* The HS_xxx values ... */
#define HS_USER  1	/* ...must start at 0... */
#define HS_HOST  2	/* ...and increment w/o holes... */
#define HS_GROUP 3	/* ...to the largest value */

#define HS_SETS  4	/* Number of HS_xxx items, above. */

#endif
