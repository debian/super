/* Functions for processing option strings are OptionFunc's, and they
 * have the following arguments:
 *	wd	-- the entire "xxx<SEP>yyy" string;
 *	s	-- points to "yyy" in wd;
 *	global	-- is !0 if this is a global invocation.
 */
typedef int (*OptionFunc) P__((char *wd, char *s, int global));

/* Flags for use in the options description */
#define		GLOBAL		002 /* option is permitted as global opt */
#define		LOCAL		004 /* option is permitted as local opt */
#define		LIST		010 /* Run globbraces on argument, then
				     * expanded list as well as string.
				     */

struct option {
    char *name;			/* name of option */
    int flags;
    OptionFunc process;		/* Function to process this option */
};
typedef struct option Option;

/* Option-processing functions */

int option_clear_settings P__(( char *word, char *s, int isglobal ));
int option_global_reset_settings P__(( void ));
int option_local_clear_settings P__(( void ));

static int option_arg		P__(( char *word, char *p, int isglobal ));
static int option_argv0		P__(( char *word, char *s, int isglobal ));
static int option_auth		P__(( char *word, char *s, int isglobal ));
static int option_authprompt	P__(( char *word, char *s, int isglobal ));
static int option_authtype	P__(( char *word, char *s, int isglobal ));
static int option_authuser	P__(( char *word, char *s, int isglobal ));
static int option_cd		P__(( char *word, char *s, int isglobal ));
static int option_checkvar	P__(( char *word, char *s, int isglobal ));
static int option_die		P__(( char *word, char *s, int isglobal ));
static int option_egid		P__(( char *word, char *s, int isglobal ));
static int option_env		P__(( char *word, char *s, int isglobal ));
static int option_euid		P__(( char *word, char *s, int isglobal ));
static int option_fd		P__(( char *word, char *s, int isglobal ));
static int option_gethostbyname	P__(( char *word, char *s, int isglobal ));
static int option_gid		P__(( char *word, char *s, int isglobal ));
static int option_group_slash	P__(( char *word, char *s, int isglobal ));
static int option_groups	P__(( char *word, char *s, int isglobal ));
static int option_info		P__(( char *word, char *s, int isglobal ));
static int option_lang		P__(( char *word, char *s, int isglobal ));
static int option_logfile	P__(( char *word, char *s, int isglobal ));
static int option_loguid	P__(( char *word, char *s, int isglobal ));
static int option_mail		P__(( char *word, char *s, int isglobal ));
static int option_mailany	P__(( char *word, char *s, int isglobal ));
static int option_nargs		P__(( char *word, char *s, int isglobal ));
static int option_maxenvlen	P__(( char *word, char *s, int isglobal ));
static int option_maxlen	P__(( char *word, char *s, int isglobal ));
static int option_nice		P__(( char *word, char *s, int isglobal ));
static int option_owner		P__(( char *word, char *s, int isglobal ));
static int option_password	P__(( char *word, char *s, int isglobal ));
static int option_patterns	P__(( char *word, char *s, int isglobal ));
static int option_print		P__(( char *word, char *s, int isglobal ));
static int option_relative_path	P__(( char *word, char *s, int isglobal ));
static int option_renewtime	P__(( char *word, char *s, int isglobal ));
/* static int option_rgid		P__(( char *word, char *s, int isglobal )); */
static int option_rlog_host	P__(( char *word, char *s, int isglobal ));
/* static int option_ruid		P__(( char *word, char *s, int isglobal )); */
static int option_setenv	P__(( char *word, char *s, int isglobal ));
static int option_syslog	P__(( char *word, char *s, int isglobal ));
static int option_syslog_error	P__(( char *word, char *s, int isglobal ));
static int option_syslog_success P__(( char *word, char *s, int isglobal ));
static int option_timeout	P__(( char *word, char *s, int isglobal ));
static int option_timestampbyhost P__(( char *word, char *s, int isglobal ));
static int option_timestampuid	P__(( char *word, char *s, int isglobal ));
static int option_u_g		P__(( char *word, char *s, int isglobal ));
static int option_uid		P__(( char *word, char *s, int isglobal ));
static int option_umask		P__(( char *word, char *s, int isglobal ));
